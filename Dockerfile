ARG PHP_VERSION

FROM unicaen-dev-php${PHP_VERSION}-apache

LABEL maintainer="Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>"

WORKDIR /var/www/html

ENV APACHE_CONF_DIR=/etc/apache2 \
    PHP_CONF_DIR=/etc/php/${PHP_VERSION}

## Installation de packages requis.
RUN apt-get update -qq && \
    apt-get install -y \
        php${PHP_VERSION}-pgsql

# Nettoyage
RUN apt-get autoremove -y && apt-get clean && rm -rf /tmp/* /var/tmp/*

# Symlink apache access and error logs to stdout/stderr so Docker logs shows them
RUN ln -sf /dev/stdout /var/log/apache2/access.log
RUN ln -sf /dev/stdout /var/log/apache2/other_vhosts_access.log
RUN ln -sf /dev/stderr /var/log/apache2/error.log

# Configuration Apache et FPM
ADD docker/apache/000-default.conf ${APACHE_CONF_DIR}/sites-available/
ADD docker/php/fpm/conf.d/*.ini ${PHP_CONF_DIR}/fpm/conf.d/
ADD docker/php/cli/conf.d/*.ini ${PHP_CONF_DIR}/cli/conf.d/

RUN a2enconf php${PHP_VERSION}-fpm.conf && \
    service php${PHP_VERSION}-fpm reload
