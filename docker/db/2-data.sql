
-- Données

INSERT INTO user_role (id, role_id, is_default, parent_id) VALUES
(1, 'Standard', 1, NULL),
(2, 'Gestionnaire', 0, 1),
(3, 'Super-gestionnaire', 0, 2),
(4, 'Administrateur', 0, 3);

INSERT INTO categorie_privilege (id, code, libelle, ordre) VALUES
(1, 'droit', 'Gestion des droits', 1);

INSERT INTO privilege (id, categorie_id, code, libelle, ordre) VALUES
(1, 1, 'role-visualisation', 'Rôles - Visualisation', 1),
(2, 1, 'role-edition', 'Rôles - Édition', 2),
(3, 1, 'privilege-visualisation', 'Privilèges - Visualisation', 3),
(4, 1, 'privilege-edition', 'Privilèges - Édition', 4);

INSERT INTO role_privilege (role_id, privilege_id) VALUES
(4, 1),
(4, 2),
(4, 3),
(4, 4);

INSERT INTO "user" (username, email, display_name, password, state) VALUES
    -- utilisateur demo/azerty
    ('demo', 'demo@mail.fr', 'Demo Crite', '$2y$10$PxXnVLYnGEzEnfqPqRKJSe9AabocES2H4bBK5VzzJlzuj1rVt7Lwu', 1);

INSERT INTO user_role_linker(user_id, role_id)
SELECT u.id, r.id FROM "user" u, user_role r WHERE u.username = 'demo' and r.role_id = 'Standard';
