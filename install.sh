#!/usr/bin/env bash

CURDIR=$(cd `dirname $0` && pwd)
cd ${CURDIR}

# Composer install
composer install --no-dev --no-suggest --prefer-dist --optimize-autoloader

# Commandes Doctrine
vendor/bin/doctrine-module orm:clear-cache:query
vendor/bin/doctrine-module orm:clear-cache:metadata
vendor/bin/doctrine-module orm:clear-cache:result
vendor/bin/doctrine-module orm:generate-proxies
