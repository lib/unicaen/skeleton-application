# Squelette d'application Unicaen

Ce squelette d'application constitue un socle minimal pour entamer rapidement le développement d'une nouvelle 
application à la mode "Unicaen".


## Création d'une nouvelle appli à partir du squelette

Imaginons que nous voulons créer une nouvelle appli PHP 7.3 dans le répertoire `./newapp`.

### Préalable

Nous aurons besoin de l'image Docker `unicaen-dev-php${PHP_VERSION}-apache` pour être dans un
environnement PHP dont on maîtrise la version. Pour obtenir puis construire cette image, reportez-vous au dépôt 
[unicaen-image](https://git.unicaen.fr/open-source/docker/unicaen-image).

### Création des sources de l'application

- Pour obtenir le master des sources du squelette d'application :

```bash
APP_DIR=newapp

git clone git@git.unicaen.fr:lib/unicaen/skeleton-application.git ${APP_DIR}
cd ${APP_DIR}
```

- Puis pour installer les dépendances PHP :

```bash
# si vous avez la joie d'être derrière un proxy :
#HTTP_PROXY=http://10.14.128.99:3128
#HTTPS_PROXY=${HTTP_PROXY}
#NO_PROXY=unicaen.fr,localhost

PHP_VERSION=7.3

docker run \
--rm \
-v $PWD:/app \
-w /app \
--env HTTP_PROXY \
--env HTTPS_PROXY \
--env NO_PROXY \
unicaen-dev-php${PHP_VERSION}-apache \
composer install
```

- Puis pour installer les fichiers de config exemples :

```bash
docker run \
--rm \
-v $PWD:/app \
-w /app \
--env HTTP_PROXY \
--env HTTPS_PROXY \
--env NO_PROXY \
unicaen-dev-php${PHP_VERSION}-apache \
composer run-script "post-create-project-cmd"
```

### Droits d'écriture

Comme les commandes lancées via Docker (comme ici, `composer`) sont exécutées en `root`, il faut reprendre la main 
sur les fichiers créés.

```bash
sudo chown -R ${USER}:${USER} .
```

### Création du dépôt git

```bash
git init
git add .
git commit -m "Squelette de l'application"
git push --set-upstream git@git.unicaen.fr:dsi/${APP_DIR}.git master
```

*NB : la création du dépôt git de cette façon n'est possible que si vous disposez des droits suffisants dans le 
namespace gitlab spécifié.*


## Construction et lancement du container Docker

```bash
docker-compose up --build
```


## Module Demo

Le squelette d'application possède un module "démo" qui utilise une base de données PostgreSQL de démonstration 
permettant d'avoir une authentification locale qui fonctionne. 

Cette base de données est fournie par le service `db` (fichier de config `docker-compose.yml`).
Il y a également un service `adminer` fournissant de quoi explorer la base de données avec l'outil 
["Adminer"](https://www.adminer.org) en vous rendant à l'adresse `http://localhost:9080` (sélectionner "PostgeSQL"
et utiliser les informations de connexion à la bdd présentes dans le `docker-compose.yml`).
Pour explorer/administrer la base de données *de l'extérieur du container* (avec PHPStorm par exemple), 
l'adresse de la base est cette fois `localhost:8432`.

NB :
- Grâce au montage `./data/db:/var/lib/postgresql/data` spécifié dans `docker-compose.yml`, la base de données est 
  persistée dans le répertoire `data/db` de l'appli (d'ailleurs, ne pas oublier de faire un `sudo chmod -R 777 data/db`).
- Les scripts de création de la base de données exécutés au lancement du container (si la base n'existe pas déjà)
  se trouvent dans le répertoire `./docker/db` de l'appli et sont fournis au container grâce au montage 
  `./docker/db/:/docker-entrypoint-initdb.d/`. 


## Configuration du projet

- Si cela n'a pas déjà été fait par une des `post-create-project-cmd` du `composer.json`, 
créez le fichier de config `config/autoload/local.php` à partir du `.dist` :
```bash
cp -r config/autoload/local.php.dist config/autoload/local.php
```

- Si cela n'a pas déjà été fait par une des `post-create-project-cmd` du `composer.json`, 
créez dans `config/autoload` les fichiers de configuration locaux et globaux des bibliothèques utilisées 
à partir des `.dist` :
```bash
cp -r vendor/unicaen/app/config/unicaen-app.global.php.dist   config/autoload/unicaen-app.global.php
cp -r vendor/unicaen/app/config/unicaen-app.local.php.dist    config/autoload/unicaen-app.local.php
cp -r vendor/unicaen/auth/config/unicaen-auth.global.php.dist config/autoload/unicaen-auth.global.php
cp -r vendor/unicaen/auth/config/unicaen-auth.local.php.dist  config/autoload/unicaen-auth.local.php
```

- Le cas échéant, reportez-vous aux docs des modules concernés pour adapter ces fichiers de configuration 
à vos besoins :
  - [unicaen/app](https://git.unicaen.fr/lib/unicaen/app)
  - [unicaen/auth](https://git.unicaen.fr/lib/unicaen/auth)


## Test de l'application 

Théoriquement, l'application devrait être accessible à l'adresse [https://localhost:8443](https://localhost:8443).
Le port utilisé dépend des redirections configurées dans le fichier [docker-compose.yml](docker-compose.yml).
