<?php

$modules = [
    'Laminas\Cache',
    'Laminas\Filter',
    'Laminas\Form',
    'Laminas\Hydrator',
    'Laminas\I18n',
    'Laminas\InputFilter',
    'Laminas\Log',
    'Laminas\Mail',
    'Laminas\Mvc\Console',
    'Laminas\Mvc\I18n',
//    'Laminas\Mvc\Plugin\FilePrg',
    'Laminas\Mvc\Plugin\FlashMessenger',
//    'Laminas\Mvc\Plugin\Identity',
    'Laminas\Mvc\Plugin\Prg',
    'Laminas\Navigation',
    'Laminas\Paginator',
    'Laminas\Router',
    'Laminas\Session',
    'Laminas\Validator',

    'DoctrineModule',
    'DoctrineORMModule',
    'ZfcUser',
    'BjyAuthorize',
    'UnicaenApp',
    'UnicaenAuth',
    'Application',
    'Demo',
];

$applicationEnv = getenv('APPLICATION_ENV') ?: 'production';
if ('development' === $applicationEnv) {
    $modules[] = 'Laminas\DeveloperTools';
//    $modules[] = 'UnicaenCode';
}

$moduleListenerOptions = [
    'config_glob_paths'    => [
        'config/autoload/{,*.}{global,local}.php',
    ],
    'module_paths' => [
        './module',
        './vendor',
    ],
];

return [
    'modules' => $modules,
    'module_listener_options' => $moduleListenerOptions,
];