# Module Démo Unicaen

## Création de la base données de démo

Créez la base de données sqlite3 de demo [`data/db/demo.sqlite`](data/db/demo.sqlite) :

    $ bin/create_demo_db.sh

Vérifiez la création du schéma :
    
    $ sqlite3 data/db/demo.sqlite ".schema"
    CREATE TABLE user
    (
        id           INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        username     VARCHAR(255) DEFAULT NULL,
        email        VARCHAR(255) DEFAULT NULL,
        display_name VARCHAR(64)  DEFAULT NULL,
        password     VARCHAR(128)                      NOT NULL,
        state        SMALLINT     default 1
    , PASSWORD_RESET_TOKEN varchar2(256) DEFAULT NULL);
    CREATE UNIQUE INDEX user_unique_username ON user(username);
    CREATE UNIQUE INDEX USER_PASSWORD_RESET_TOKEN_UN ON user (PASSWORD_RESET_TOKEN);
    CREATE TABLE user_role (
        id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        role_id VARCHAR(64) NOT NULL,
        is_default TINYINT(1) NOT NULL DEFAULT 0,
        parent_id INTEGER NULL DEFAULT NULL,
        ldap_filter varchar(255) DEFAULT NULL,
    
        FOREIGN KEY (parent_id) REFERENCES user_role (id) ON DELETE SET NULL
    );
    CREATE UNIQUE INDEX role_unique_role_id ON user_role(role_id);
    CREATE INDEX role_idx_parent_id ON user_role(parent_id);
    CREATE TABLE user_role_linker (
        user_id INTEGER NOT NULL,
        role_id INTEGER NOT NULL,
        PRIMARY KEY (user_id, role_id),
        FOREIGN KEY (role_id) REFERENCES user_role (id) ON DELETE CASCADE,
        FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE
    );
    CREATE INDEX user_role_linker_idx_role_id ON user_role_linker(role_id);
    CREATE INDEX user_role_linker_idx_user_id ON user_role_linker(user_id);
    CREATE TABLE categorie_privilege (
        id            INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        code          VARCHAR(150) NOT NULL,
        libelle       VARCHAR(200) NOT NULL,
        ordre         INTEGER
    );
    CREATE UNIQUE INDEX categorie_unique_code ON categorie_privilege(code);
    CREATE TABLE privilege (
        id            INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        categorie_id  INTEGER NOT NULL,
        code          VARCHAR(150) NOT NULL,
        libelle       VARCHAR(200) NOT NULL,
        ordre         INTEGER,
        FOREIGN KEY (categorie_id) REFERENCES categorie_privilege (id) ON DELETE CASCADE
    );
    CREATE UNIQUE INDEX privilege_unique_code ON privilege(code);
    CREATE TABLE role_privilege (
        role_id       INTEGER NOT NULL,
        privilege_id  INTEGER NOT NULL,
        PRIMARY KEY (role_id,privilege_id),
        FOREIGN KEY (role_id) REFERENCES user_role (id) ON DELETE CASCADE,
        FOREIGN KEY (privilege_id) REFERENCES privilege (id) ON DELETE CASCADE
    );
    CREATE INDEX idx_role_id on role_privilege(role_id);
    CREATE INDEX idx_privilege_id on role_privilege(privilege_id);

Interrogez la table `user` pour vérifier la présence de l'utilisateur `demo`, exemple :
    
    $ sqlite3 data/db/demo.sqlite "select * from user;"
    1|demo|demo@mail.fr|Demo|$2y$10$GmGzPXsLK5Kts30ZrS9QnOLsNYDgZ62797Kitp4Z1nWHlB1g7DZma|1|

NB: l'utilisateur `admin` a pour mot de passe `admin`, chiffré avec Bcrypt comme ceci : 
`php --run 'require "vendor/autoload.php"; $bcrypt = new Laminas\Crypt\Password\Bcrypt(); var_dump($bcrypt->create("xxx"));'`


## Configuration du module

- Les fichiers de configuration locaux et globaux des bibliothèques utilisées se trouvent dans 
  [`config/other`](config/other).

- Reportez-vous aux docs des modules concernés pour personnaliser ces fichiers de configuration selon vos besoins :
  - [unicaen/app](https://git.unicaen.fr/lib/unicaen/app)
  - [unicaen/auth](https://git.unicaen.fr/lib/unicaen/auth)
  - [unicaen/code](https://git.unicaen.fr/lib/unicaen/code)

- La configuration Doctrine d'acc_s à la base de données se trouve dans 
  [`config/module.config.php`](config/module.config.php).
