<?php

namespace Demo;

return [
    //
    // ATTENTION: config Doctrine à déplacer dans `APP/config/autoload/`.
    //
    'doctrine' => [
        'connection' => [
            'orm_default' => [
                'driverClass' => \Doctrine\DBAL\Driver\PDOPgSql\Driver::class,
                'params' => [
                    'host' => 'db',        //
                    'user' => 'admin',     // cf. docker-compose.yml
                    'password' => 'admin', //
                    'dbname' => 'demo',    //
                    'port' => '5432',
                    'charset' => 'utf8',
                    'driverOptions' => [1002 => 'SET NAMES utf8']
                ],
            ],
        ],
    ],

    'bjyauthorize'    => [
        'guards'                => [
            'BjyAuthorize\Guard\Controller'         => [
                ['controller' => 'Demo\Controller\Index', 'roles' => 'guest'],
            ],
        ],
    ],
    'router'          => [
        'routes' => [
            'home'        => [
                'type'    => 'Literal',
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'controller' => 'Demo\Controller\Index', // <-- change here
                        'action'     => 'index',
                    ],
                ],
            ],
        ],
    ],
    'service_manager' => [
        'factories' => [

        ],
    ],
    'translator'      => [
        'locale'                    => 'fr_FR', // en_US
        'translation_file_patterns' => [
            [
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ],
        ],
    ],
    'controllers'     => [
        'invokables' => [
            'Demo\Controller\Index' => Controller\IndexController::class,
        ],
    ],
    'view_manager'    => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
];
